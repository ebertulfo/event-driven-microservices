const express = require("express");
const redis = require('redis')
const bodyParser = require("body-parser");

const app = express();
const port = process.env.port || 8000;

const pub = redis.createClient( { host: "localhost", port: 6379 } );
const sub = redis.createClient( { host: "localhost", port: 6379 } );

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/sports', (req, res) => {
    //Emit Event that a sport needs to be created
    pub.publish("sports", JSON.stringify({event: "SPORT_CREATED", payload: req.body}));
    
    //Listen for Event that the the sport has been created
    sub.on("message", (channel, message) => {
        message = JSON.parse(message);
        if(message.event == "SPORT_SAVED") {
            res.send({status: 200, msg: "Sport has been saved"});
        }
    })
    
})

sub.on("ready", ()=>{
    sub.subscribe("sports");

    app.listen(port, () => {
        console.log(`API Gateway Running in port ${port}`);
    });
})


